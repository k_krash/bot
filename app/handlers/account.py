from aiogram import types, Dispatcher

from app.MySQL.connect import get_user_payment, get_created_time, get_until


async def account(message: types.Message):
    ID = str(message.from_user.id)
    created_date = str(get_created_time(message.from_user.id))[20:31].replace(' ', '')
    default_date = created_date.replace(',', '-')
    end_date = str(get_until(message.from_user.id))[20:31].replace(' ', '')
    finish_date = end_date.replace(',', '-')
    if get_user_payment(message.from_user.id): #and datatime.datetime.now() =< det_until(message.from_user.id)
        subscribe = 'Активна✅'
        salt = "<b>Действует до: " + finish_date + "</b>"
    else:
        subscribe = 'Не активна❌'
        salt = ''
    await message.answer(
        "<b>👤Айди: " + ID + "</b>\n"
        "<b>📆 Дата Регистрации: " + default_date + "</b>\n"
        "<b>💳 Подписка: " + subscribe + "</b>\n" + salt
        , parse_mode=types.ParseMode.HTML)


def register_handlers_account(dp: Dispatcher):
    dp.register_message_handler(account, text="👤Аккаунт", state="*")
