from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text
from aiogram.types import KeyboardButton

from app.murkups import get_start_keyboard


async def cmd_start(message: types.Message, state: FSMContext):
    await state.finish()
    keyboard = get_start_keyboard(message.from_user.id)
    await message.answer(keyboard['text'], reply_markup=keyboard['keyboard'])


async def cmd_cancel(message: types.Message):
    await message.answer("Клавиатура спрятана!", reply_markup=types.ReplyKeyboardRemove())


def register_handlers_common(dp: Dispatcher):
    dp.register_message_handler(cmd_start, commands="start", state="*")
    dp.register_message_handler(cmd_start, text="Перейти в основной режим", state="open")
    dp.register_message_handler(cmd_cancel, commands="cancel")
    dp.register_message_handler(cmd_cancel, Text(equals="отмена", ignore_case=True), state="*")
