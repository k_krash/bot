from aiogram import Dispatcher, types, Bot

from aiogram.dispatcher import FSMContext
from aiogram.types import ReplyKeyboardRemove
from glQiwiApi import types as qiwi_types
from typing import Union

from glQiwiApi.types import Bill

from app.MySQL.connect import add_user
from app.qiwy.connect import wallet

verify_menu = [types.InlineKeyboardButton(text="Оплатил", callback_data='verify')]
open_menu = [types.KeyboardButton("Перейти в основной режим")]

convert_ct = {'500': 'день',
              '2500': 'неделю',
              '4000': 'месяц'}

keyboard_inline = types.InlineKeyboardMarkup()
keyboard_inline.add(*verify_menu)
keyboard_reply = types.ReplyKeyboardMarkup(resize_keyboard=True)
keyboard_reply.add(*open_menu)


async def create_payment(amount: Union[float, int]) -> qiwi_types.Bill:
    async with wallet as w:
        return await w.create_p2p_bill(amount)


async def subscription(message: types.Message, state: FSMContext):
    await state.set_state('pay')
    costs = [types.InlineKeyboardButton(text='⚜️День⚜️', callback_data='500'),
             types.InlineKeyboardButton(text='⚜ Неделя⚜ ', callback_data='2500'),
             types.InlineKeyboardButton(text='⚜ Месяц⚜ ', callback_data='4000'),
             types.InlineKeyboardButton(text='Выбрать другой тип оплаты', callback_data='back')]
    keyboard_markup = types.InlineKeyboardMarkup(row_width=3)
    keyboard_markup.add(*costs)
    await message.answer("1 сутки - 500рублей\n 7 суток - 2500 рублей\n 30 суток - 4000 рублей\n Выберите тариф:",
                         reply_markup=keyboard_markup)


async def payment(call: types.CallbackQuery, state: FSMContext):
    bill = await create_payment(call.data)
    pay = await state.get_state()
    message_ID = call.message.message_id + 1
    if pay == 'pay':
        await call.message.answer(f'Хорошо, вот счёт для оплаты на {convert_ct[call.data]}:\n {bill.pay_url}',
                                  reply_markup=keyboard_inline)
    elif pay == 'edit':
        await call.bot.edit_message_text(text=f'Хорошо, вот счёт для оплаты на {convert_ct[call.data]}:\n{bill.pay_url}',
                                         chat_id=call.message.chat.id, message_id=message_ID, reply_markup=keyboard_inline)
    await state.set_state('payment '+call.data)
    await state.update_data(bill=bill)


async def successful_payment(call: types.CallbackQuery, state: FSMContext):
    #async with state.proxy() as data:
    #    bill: Bill = data.get('bill')
    #status = await bill.check()
    #if status:
        await call.message.answer(f"Вы успешно оплатили счёт"+str(await state.get_state()), reply_markup=keyboard_reply)
        add_user(call.from_user.id, '1', await state.get_state())
        await state.set_state('open')
    #else:
     #   await call.message.answer("Счёт не оплачен")


async def back(call: types.CallbackQuery, state: FSMContext):
    await state.finish()
    await call.answer("Действие отменено")
    await state.set_state('edit')


def register_handlers_qiwi(dp: Dispatcher):
    dp.register_callback_query_handler(payment, lambda c: c.data in ['500', '2500', '4000'], state='pay')
    dp.register_callback_query_handler(payment, state='edit')
    dp.register_callback_query_handler(successful_payment, lambda c: c.data == 'verify', state='payment 500')
    dp.register_callback_query_handler(successful_payment, lambda c: c.data == 'verify', state='payment 2500')
    dp.register_callback_query_handler(successful_payment, lambda c: c.data == 'verify', state='payment 4000')

    dp.register_callback_query_handler(back, lambda c: c.data == 'back', state="payment")
    dp.register_message_handler(subscription, text="💳 Подписка", state="*")
    dp.register_message_handler(subscription, text="💳 Продление", state="*")
