from aiogram import types, Dispatcher


async def support(message: types.Message):
    await message.answer('Send message to @BartHack')


def register_handlers_support(dp: Dispatcher):
    dp.register_message_handler(support, text="🥷🏿Техподдержка", state="*")
