from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext


async def FAQ(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer(text='Как работает бот? \n- Мы работаем с несколькими каталогами ресурсов, по которым идёт '
                              'поиск\nПочему так дорого? \n- По тому, что в отличии от конкурентов мы делаем более '
                              'обширный поиск. \nОплатил, а подписка на появилась. Что делать? \n- Напишите в поле '
                              '"Тех-Поддержка"\nЯ хочу купить подписку, но боюсь что меня обманут. \n- Вы можете '
                              'купить подписку через гаранта, написав глав.админу')


def register_handlers_faq(dp: Dispatcher):
    dp.register_message_handler(FAQ, text="❓ FAQ", state="*")
    dp.register_message_handler(FAQ, commands="FAQ")

