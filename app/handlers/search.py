from aiogram import types, Dispatcher


async def search(message: types.Message):
    await message.answer('This is search🔍')


def register_handlers_search(dp: Dispatcher):
    dp.register_message_handler(search, text="🔍Поиск", state="*")
