from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, base


def get_ReplyButton(arguments, resize_keyboard: base.Boolean = False,
                    one_time_keyboard: base.Boolean = False,
                    input_field_placeholder: base.String = '',
                    selective: base.Boolean = False,
                    row_width: base.Integer = 3):
    keyboard_markup = ReplyKeyboardMarkup(resize_keyboard, one_time_keyboard, input_field_placeholder, selective,
                                          row_width)
    for argument in arguments:
        keyboard_markup.add(KeyboardButton(argument))
    return keyboard_markup



