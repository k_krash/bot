from aiogram.types import KeyboardButton, ReplyKeyboardMarkup

from app.MySQL.connect import get_user_payment, add_user, get_user

available_verify_menu = [KeyboardButton(text="🔍Поиск"),
                         KeyboardButton(text="💳 Продление"),
                         KeyboardButton(text="❓ FAQ"),
                         KeyboardButton(text="👤Аккаунт"),
                         KeyboardButton(text="🥷🏿Техподдержка"),
                         ]
available_guest_menu = [KeyboardButton(text="💳 Подписка"),
                        KeyboardButton(text="❓ FAQ"),
                        KeyboardButton(text="👤Аккаунт"),
                        ]


def get_start_keyboard(user_id):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    if get_user_payment(user_id): #and datatime.datetime.now() =< det_until(message.from_user.id)
        keyboard.add(*available_verify_menu)
        text = "Welcome!"
    else:
        keyboard.add(*available_guest_menu)
        if not get_user(user_id):
            add_user(user_id, '0')
        text = "Привет!\tЭто бот для поиска информации. Просто введи её! Ты получишь обширный отчёт."
    answer = {'text': text, 'keyboard': keyboard}
    return answer