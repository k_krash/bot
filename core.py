#! /usr/bin/env python
# -*- coding: utf-8 -*-

from aiogram import Bot, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import Dispatcher
from aiogram.types import BotCommand
from os.path import join

from app.handlers.FAQ import register_handlers_faq
from app.handlers.account import register_handlers_account
from app.handlers.qiwi import register_handlers_qiwi
from app.handlers.search import register_handlers_search
from app.handlers.support import register_handlers_support
from config_reader import load_config

import asyncio
import logging


from app.handlers.common import register_handlers_common
from app.handlers.food import register_handlers_food

logger = logging.getLogger(__name__)


# Регистрация команд, отображаемых в интерфейсе Telegram
async def set_commands(bot: Bot):
    commands = [
        BotCommand(command="/start", description="Запустить бота"),
        BotCommand(command="/faq", description="Показать FAQ"),
        BotCommand(command="/cancel", description="Спрятать клавиатуру"),
        BotCommand(command="/open", description="Показать клавиатуру")
    ]
    await bot.set_my_commands(commands)



async def main():
    # Настройка логирования в stdout
    logging.basicConfig(
        filename="log_base.log",
        level=logging.DEBUG,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    )

    log = logging.getLogger("bot")
    logger.error("Starting bot")

    # Парсинг файла конфигурации
    config = load_config(join("config", "bot.ini"))

    # Объявление и инициализация объектов бота и диспетчера
    bot = Bot(token=config.tg_bot.token, parse_mode=types.ParseMode.HTML)
    dp = Dispatcher(bot, storage=MemoryStorage())

    # Регистрация хэндлеров
    register_handlers_common(dp)
    register_handlers_food(dp)
    register_handlers_qiwi(dp)
    register_handlers_faq(dp)
    register_handlers_support(dp)
    register_handlers_search(dp)
    register_handlers_account(dp)

    # Установка команд бота
    await set_commands(bot)

    # Запуск поллинга
    await dp.skip_updates()  # пропуск накопившихся апдейтов (необязательно)
    await dp.start_polling()


if __name__ == '__main__':
    print("start")
    asyncio.run(main())
